import idv.kevin.ssm.domain.Permission;
import idv.kevin.ssm.domain.SsmUser;
import idv.kevin.ssm.mapper.SsmUserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class UserMapperTest {

    @Autowired
    private SsmUserMapper userMapper;

    @Test
    public void testFindByUserName() {
        SsmUser user = userMapper.findByUserName("Eric");
        System.out.println(user);
    }

    @Test
    public void testFindPermissionByUserName() {
        List<Permission> permissions = userMapper.findPermissionByUserName("Jack");
        permissions.forEach(p -> {
            System.out.println(p.getPermission_name() + "-" + p.getPermission_tag());
        });
    }


    @Test
    public void testUpdatePassword() {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        SsmUser ssmUser = new SsmUser();
        ssmUser.setUser_name("Eric");
        ssmUser.setPassword(passwordEncoder.encode("123456"));
        userMapper.updatePassword(ssmUser);
    }
}
