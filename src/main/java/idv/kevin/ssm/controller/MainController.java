package idv.kevin.ssm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @GetMapping("/userLogin")
    public String login() {

        return "login";
    }


    @GetMapping("/error")
    public String error() {

        return "errorPage";
    }

    @GetMapping("loginFail")
    public String loginFail() {

        return "loginFailPage";
    }

}
