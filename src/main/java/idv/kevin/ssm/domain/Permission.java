package idv.kevin.ssm.domain;

public class Permission {
    private Integer permission_id;
    private String permission_name;
    private String permission_tag;

    public Integer getPermission_id() {
        return permission_id;
    }

    public void setPermission_id(Integer permission_id) {
        this.permission_id = permission_id;
    }

    public String getPermission_name() {
        return permission_name;
    }

    public void setPermission_name(String permission_name) {
        this.permission_name = permission_name;
    }

    public String getPermission_tag() {
        return permission_tag;
    }

    public void setPermission_tag(String permission_tag) {
        this.permission_tag = permission_tag;
    }
}
