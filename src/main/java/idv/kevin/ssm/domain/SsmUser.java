package idv.kevin.ssm.domain;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class SsmUser implements UserDetails {
    private Integer user_Id; //int(10) NOT NULL AUTO_INCREMENT,
    private String user_name; //varchar(50) NOT NULL,
    private String user_real_name; //varchar(50) NOT NULL,
    private String password; //varchar(50) NOT NULL,
    private Date gmt_create; //timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    private Date gmt_last_login; //timestamp NULL DEFAULT NULL,
    private boolean enabled; //int(5) NOT NULL,
    private boolean account_non_expired; //int(5) NOT NULL,
    private boolean account_non_locked; //int(5) NOT NULL,
    private boolean credential_non_expired; //int(5) NOT NULL,

    private List<GrantedAuthority> authorities = new ArrayList<>();

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return user_name;
    }

    @Override
    public boolean isAccountNonExpired() {
        return account_non_expired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return account_non_locked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credential_non_expired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public Integer getUser_Id() {
        return user_Id;
    }

    public void setUser_Id(Integer user_Id) {
        this.user_Id = user_Id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_real_name() {
        return user_real_name;
    }

    public void setUser_real_name(String user_real_name) {
        this.user_real_name = user_real_name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getGmt_create() {
        return gmt_create;
    }

    public void setGmt_create(Date gmt_create) {
        this.gmt_create = gmt_create;
    }

    public Date getGmt_last_login() {
        return gmt_last_login;
    }

    public void setGmt_last_login(Date gmt_last_login) {
        this.gmt_last_login = gmt_last_login;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isAccount_non_expired() {
        return account_non_expired;
    }

    public void setAccount_non_expired(boolean account_non_expired) {
        this.account_non_expired = account_non_expired;
    }

    public boolean isAccount_non_locked() {
        return account_non_locked;
    }

    public void setAccount_non_locked(boolean account_non_locked) {
        this.account_non_locked = account_non_locked;
    }

    public boolean isCredential_non_expired() {
        return credential_non_expired;
    }

    public void setCredential_non_expired(boolean credential_non_expired) {
        this.credential_non_expired = credential_non_expired;
    }

    public void setAuthorities(List<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    @Override
    public String toString() {
        return "User{" +
                "user_Id=" + user_Id +
                ", user_name='" + user_name + '\'' +
                ", user_real_name='" + user_real_name + '\'' +
                ", password='" + password + '\'' +
                ", gmt_create=" + gmt_create +
                ", gmt_last_login=" + gmt_last_login +
                ", enabled=" + enabled +
                ", account_non_expired=" + account_non_expired +
                ", account_non_locked=" + account_non_locked +
                ", credential_non_expired=" + credential_non_expired +
                ", authorities=" + authorities +
                '}';
    }
}
