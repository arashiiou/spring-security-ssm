package idv.kevin.ssm.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import idv.kevin.ssm.domain.SsmUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SsmAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private ObjectMapper objectMapper = new ObjectMapper();
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

        SsmUser ssmUser = (SsmUser) (authentication.getPrincipal());
        logger.info("user " + ssmUser.getUser_name() + " login success");

        Map result = new HashMap();
        result.put("success", true);

        String resultJson = objectMapper.writeValueAsString(result);
        response.setContentType("text/json;charset=utf-8");
        response.getWriter().write(resultJson);

    }
}
