package idv.kevin.ssm.handler;

import idv.kevin.ssm.domain.SsmUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SsmLogoutSuccessHandler implements LogoutSuccessHandler {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

        SsmUser ssmUser = (SsmUser) (authentication.getPrincipal());
        logger.info("user " + ssmUser.getUser_name() + " logout success");
        response.sendRedirect("product/index");
    }
}
