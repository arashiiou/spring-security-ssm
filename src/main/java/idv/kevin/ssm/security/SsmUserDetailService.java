package idv.kevin.ssm.security;

import idv.kevin.ssm.domain.Permission;
import idv.kevin.ssm.domain.SsmUser;
import idv.kevin.ssm.mapper.SsmUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class SsmUserDetailService implements UserDetailsService {

//    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private SsmUserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SsmUser user = userMapper.findByUserName(username);

        List<Permission> permissions = userMapper.findPermissionByUserName(username);

        List<GrantedAuthority> authorities = new ArrayList<>();

        permissions.forEach(p -> {
            GrantedAuthority authority = new SimpleGrantedAuthority(p.getPermission_tag());
            authorities.add(authority);
        });

        user.setAuthorities(authorities);

//        logger.info("user " + user.getUser_name() + " login success");

        return user;
    }
}
