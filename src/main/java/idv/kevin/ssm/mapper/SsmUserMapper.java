package idv.kevin.ssm.mapper;

import idv.kevin.ssm.domain.Permission;
import idv.kevin.ssm.domain.SsmUser;

import java.util.List;

public interface SsmUserMapper {

    public SsmUser findByUserName(String userName);

    public List<Permission> findPermissionByUserName(String userName);

    public void updatePassword(SsmUser ssmUser);
}
