<%--
  Created by IntelliJ IDEA.
  User: kittenmeow
  Date: 2019-06-12
  Time: 17:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<html>
<head>
    <title>index</title>
</head>
<body>
<sec:authorize access="!isAuthenticated()">
    <a href="${pageContext.request.contextPath}/userLogin">登入</a><br>
</sec:authorize>


<sec:authorize access="isAuthenticated()">
    ${userName}已經登入囉！
    <a href="${pageContext.request.contextPath}/logout">登出</a><br>
</sec:authorize>
<%--<form action="${pageContext.request.contextPath}/logout" method="GET">--%>
<%--<input type="submit" value="Logout" />--%>
<%--</form>--%>

<br>

<sec:authorize access="hasAuthority('Product_Add')">
    <a href="${pageContext.request.contextPath}/product/add">add</a><br>
</sec:authorize>
<sec:authorize access="hasAuthority('Product_Update')">
    <a href="${pageContext.request.contextPath}/product/update">update</a><br>
</sec:authorize>
<sec:authorize access="hasAuthority('Product_Delete')">
    <a href="${pageContext.request.contextPath}/product/delete">delete</a><br>
</sec:authorize>
<sec:authorize access="hasAuthority('Product_List')">
    <a href="${pageContext.request.contextPath}/product/list">list</a><br>
</sec:authorize>
</body>
</html>
