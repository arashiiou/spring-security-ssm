<%--
  Created by IntelliJ IDEA.
  User: kittenmeow
  Date: 2019-06-28
  Time: 11:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登入</title>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
</head>
<body>
<%--<h3>自訂的登入頁form</h3>--%>
<%--<form action="${pageContext.request.contextPath}/login" method="POST">--%>
<%--username:<input type="text" name="username"><br>--%>
<%--password:<input type="text" name="password"><br>--%>
<%--<input type="submit" value="登入">--%>
<%--</form>--%>

<br>
<h3>自訂的登入頁ajax</h3>
<form id="loginForm">
    username:<input type="text" name="username"><br>
    password:<input type="text" name="password"><br>
    remember me: <input type="checkbox" name="remember-me" value="true"><br>
    <input type="button" id="loginBtn" value="登入">
</form>

<script>
    $('#loginBtn').click(function () {
        $.post("${pageContext.request.contextPath}/login", $('#loginForm').serialize(), function (data) {
            if (data.success) {
                window.location.href = "${pageContext.request.contextPath}/product/index";
            } else {
                alert("帳號或密碼錯誤");
            }
        }, "json")
    });
</script>
</body>
</html>
