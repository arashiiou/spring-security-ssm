DROP TABLE IF EXISTS role_permission_tbl;
DROP TABLE IF EXISTS user_role_tbl;
DROP TABLE IF EXISTS permission;
DROP TABLE IF EXISTS role;
DROP TABLE IF EXISTS users;


CREATE TABLE IF NOT EXISTS users
(
  user_id INT
(
  10
) AUTO_INCREMENT NOT NULL,
  user_name VARCHAR
(
  50
) NOT NULL,
  user_real_name VARCHAR
(
  50
) NOT NULL,
  password VARCHAR
(
  100
) NOT NULL,
  gmt_create TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  gmt_last_login TIMESTAMP NULL,
  enabled INT
(
  5
) NOT NULL,
  account_non_expired INT
(
  5
) NOT NULL,
  account_non_locked INT
(
  5
) NOT NULL,
  credential_non_expired INT
(
  5
) NOT NULL,
  PRIMARY KEY
(
  user_id
)
  );

CREATE TABLE IF NOT EXISTS role
(
  role_id INT
(
  10
) AUTO_INCREMENT NOT NULL,
  role_name VARCHAR
(
  50
) NOT NULL,
  role_desciption VARCHAR
(
  50
) NOT NULL,
  PRIMARY KEY
(
  role_id
)
  );

CREATE TABLE IF NOT EXISTS permission
(
  permission_id INT
(
  10
) AUTO_INCREMENT NOT NULL,
  permission_name VARCHAR
(
  50
) NOT NULL,
  permission_tag VARCHAR
(
  50
) NOT NULL,
  PRIMARY KEY
(
  permission_id
)
  );

CREATE TABLE IF NOT EXISTS user_role_tbl
(
  user_id INT
(
  10
),
  role_id INT
(
  10
),
  PRIMARY KEY
(
  user_id,
  role_id
),
  CONSTRAINT fk_user_id FOREIGN KEY
(
  user_id
) REFERENCES users
(
  user_id
),
  CONSTRAINT fk_role_id FOREIGN KEY
(
  role_id
) REFERENCES role
(
  role_id
)
  );

CREATE TABLE IF NOT EXISTS role_permission_tbl
(
  role_id INT
(
  10
),
  permission_id INT
(
  10
),
  PRIMARY KEY
(
  role_id,
  permission_id
),
  CONSTRAINT fk_role_id_permission FOREIGN KEY
(
  role_id
) REFERENCES role
(
  role_id
),
  CONSTRAINT fk_permission_id_permission FOREIGN KEY
(
  permission_id
) REFERENCES permission
(
  permission_id
)
  );


INSERT INTO users(user_id, user_name, user_real_name, password, gmt_last_login, enabled, account_non_expired,
                  account_non_locked, credential_non_expired)
VALUES
  (1, 'Eric', '張三', '123456', NULL, 1, 1, 1, 1),
  (2, 'Jack', '李四', '123456', NULL, 1, 1, 1, 1);

INSERT INTO role(role_id, role_name, role_desciption)
VALUES
  (1, '普通用戶', '普通用戶'),
  (2, '管理員', '管理員');

INSERT INTO permission(permission_id, permission_name, permission_tag)
VALUES
  (1, '查詢', 'List_Product'),
  (2, '新增', 'Add_Product'),
  (3, '修改', 'Update_Product'),
  (4, '刪除', 'List_Product');

INSERT INTO user_role_tbl(user_id, role_id)
VALUES
  (1, 1),
  (2, 2);

INSERT INTO role_permission_tbl(role_id, permission_id)
VALUES
  (1, 1),
  (2, 1),
  (2, 2),
  (2, 3),
  (2, 4);